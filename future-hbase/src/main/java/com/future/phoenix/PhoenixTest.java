package com.future.phoenix;

import org.junit.Before;
import org.junit.Test;

import java.sql.*;
import java.util.Properties;

/**
 * @author tangqiang 1071180223@qq.com
 * @date 2018/07/18
 */
public class PhoenixTest {
    Connection conn;
    
    @Before
    public void init() throws SQLException {
        Properties props = new Properties();
        props.setProperty("phoenix.functions.allowUserDefinedFunctions", "true");
        conn = DriverManager.getConnection("jdbc:phoenix:sandbox-hdp.hortonworks.com:2181:/hbase-unsecure", props);
    }
    
    
    @Test
    public void test1() throws Exception {
        Statement statement = conn.createStatement();
        statement.execute("CREATE TABLE demo ( id BIGINT not null primary key)");
    }
    
    @Test
    public void test2() throws Exception {
        Statement statement = conn.createStatement();
        statement.execute("DROP TABLE IF EXISTS test");
    }
    
    @Test
    public void test3() throws Exception {
        Statement statement = conn.createStatement();
        statement.execute("UPSERT INTO demo VALUES(1)");
    }
    
    @Test
    public void test4() throws Exception {
        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM demo LIMIT 1000");
        System.out.println();
    }
}
