package com.future.hbase;

import org.junit.Test;

/**
 * @author tangqiang 1071180223@qq.com
 * @date 2018/07/10
 */
public class HbaseTest {
    
    @Test
    public void create() throws Exception {
        HBaseUtils.create("test", "hello");
    }
    
    @Test
    public void put() throws Exception {
        HBaseUtils.put("test", "row_1", "hello", "column_1", "123");
    }
    
    @Test
    public void get() throws Exception {
        String data = HBaseUtils.get("test", "row_1");
        System.out.println(data);
    }
}
