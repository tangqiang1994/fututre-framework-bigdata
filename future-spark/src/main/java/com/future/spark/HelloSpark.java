package com.future.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.junit.Test;
import scala.Tuple2;

import java.util.Arrays;
import java.util.List;

/**
 * @author tangqiang 1071180223@qq.com
 * @date 2018/06/21
 */
public class HelloSpark {

    /**
     * 可以访问UI界面
     */
    @Test
    public void hello() {
        SparkConf conf = new SparkConf().setMaster("local").setAppName("HelloSpark");
        try (JavaSparkContext sc = new JavaSparkContext(conf)) {
            List<Integer> data = Arrays.asList(1, 2, 3, 4, 5);
            // sc是上述代码已经创建的JavaSparkContext实例
            JavaRDD<Integer> distData = sc.parallelize(data);
            Integer reduce = distData.reduce((a, b) -> a + b);
            System.out.println(reduce);
            Thread.sleep(Integer.MAX_VALUE);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test1() {
        SparkConf conf = new SparkConf().setMaster("local").setAppName("HelloSpark");
        try (JavaSparkContext sc = new JavaSparkContext(conf)) {
            List<Integer> data = Arrays.asList(1, 2, 3, 4, 5);
            // sc是上述代码已经创建的JavaSparkContext实例
            JavaRDD<Integer> distData = sc.parallelize(data);
            JavaRDD<Integer> javaRDD = distData.filter(item -> item > 3);
            Integer reduce = javaRDD.reduce((a, b) -> a + b);
            System.out.println(reduce);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test2() {
        SparkConf conf = new SparkConf().setMaster("local").setAppName("HelloSpark");
        try (JavaSparkContext sc = new JavaSparkContext(conf)) {
            JavaRDD<String> javaRDD = sc.textFile("log.txt");
            JavaRDD<String> error = javaRDD.filter(log -> log.contains("error"));
            long count = error.count();
            System.out.println(count);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test3() {
        SparkConf conf = new SparkConf().setMaster("local").setAppName("HelloSpark");
        try (JavaSparkContext sc = new JavaSparkContext(conf)) {
            List<String> data = Arrays.asList("1 hello world", "1 yes or no", "2 you can");
            // sc是上述代码已经创建的JavaSparkContext实例
            JavaRDD<String> distData = sc.parallelize(data);
            JavaRDD<String> stringJavaRDD = distData.flatMap(line -> Arrays.asList(line.split(" ")).iterator());
            //存储在堆内存中
            stringJavaRDD.persist(StorageLevel.DISK_ONLY());

            JavaPairRDD<String, String> pairRDD = stringJavaRDD.mapToPair(item -> new Tuple2<>(item.split(" ")[0], item));

            JavaPairRDD<String, Iterable<String>> groupRDD = pairRDD.groupByKey();

            System.out.println(groupRDD.countByKey());

            System.out.println(stringJavaRDD.count());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test4() {
        SparkConf conf = new SparkConf().setMaster("local").setAppName("HelloSpark");
        // 从SparkConf创建StreamingContext并指定1秒钟的批处理大小
        JavaStreamingContext jsc = new JavaStreamingContext(conf, Durations.seconds(1));
        // 以端口7777作为输入来源创建DStream
        JavaDStream<String> lines = jsc.socketTextStream("localhost", 7777);
        // 从DStream中筛选出包含字符串"error"的行
        JavaDStream<String> errorLines = lines.filter(line -> line.contains("error"));
        // 打印出有"error"的行
        errorLines.print();
    }
}
