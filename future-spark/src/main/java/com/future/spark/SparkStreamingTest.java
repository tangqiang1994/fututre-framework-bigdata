package com.future.spark;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;
import org.junit.Test;
import scala.Tuple2;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tangqiang
 * @date 2019-03-10
 */
public class SparkStreamingTest {

    @Test
    public void socketTest() throws InterruptedException {
        SparkConf conf = new SparkConf().setMaster("local").setAppName("SparkStreaming");
        // 从SparkConf创建StreamingContext并指定1秒钟的批处理大小
        JavaStreamingContext jssc = new JavaStreamingContext(conf, Durations.seconds(1));
        // 以端口7777作为输入来源创建DStream
        JavaDStream<String> lines = jssc.socketTextStream("localhost", 7777);
        // 从DStream中筛选出包含字符串"error"的行
        JavaDStream<String> errorLines = lines.filter((Function<String, Boolean>) line -> line.contains("error"));
        // 打印出有"error"的行
        errorLines.print();

        // 启动流计算环境StreamingContext并等待它"完成"
        jssc.start();
        // 等待作业完成
        jssc.awaitTermination();
    }

    @Test
    public void kafkaLinkTest() throws InterruptedException {
        SparkConf sparkConf = new SparkConf().setMaster("local[2]").setAppName("JavaDirectKafkaWordCount");
        JavaStreamingContext jssc = new JavaStreamingContext(sparkConf, Durations.seconds(2));

        Map<String, Object> kafkaParams = new HashMap<>();
        kafkaParams.put("bootstrap.servers", "192.168.90.130:9092");
        kafkaParams.put("key.deserializer", StringDeserializer.class);
        kafkaParams.put("value.deserializer", StringDeserializer.class);
        kafkaParams.put("group.id", "spark-streaming-group");
        kafkaParams.put("auto.offset.reset", "latest");
        kafkaParams.put("enable.auto.commit", false);

        Collection<String> topics = Arrays.asList("spark-streaming");

        JavaInputDStream<ConsumerRecord<String, String>> stream =
                KafkaUtils.createDirectStream(
                        jssc,
                        LocationStrategies.PreferConsistent(),
                        ConsumerStrategies.Subscribe(topics, kafkaParams)
                );
        JavaPairDStream<String, String> dStream = stream.mapToPair(record -> new Tuple2<>(record.key(), record.value()));

//        JavaPairDStream<String, String> error = dStream.filter(value -> value._2.contains("error"));

        dStream.print();

        // 启动流计算环境StreamingContext并等待它"完成"
        jssc.start();
        // 等待作业完成
        jssc.awaitTermination();
    }
}
