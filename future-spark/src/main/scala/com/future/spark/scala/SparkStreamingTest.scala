package com.future.spark.scala

import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.KafkaUtils
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.{Durations, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}
import org.junit.Test

/**
  *
  * @author tangqiang
  * @date 2019-03-11
  */
class SparkStreamingTest {

  @Test
  def test1: Unit = {
    val conf = new SparkConf().setMaster("local").setAppName("HelloSpark")
    val sc = new SparkContext(conf)

    val data: Array[Int] = Array(1, 2, 3, 4, 5)

    val distData = sc.parallelize(data)

    val reduce = distData.reduce((a: Int, b: Int) => a + b)

    System.out.println(reduce)

    Thread.sleep(Integer.MAX_VALUE)
  }

  @Test
  def kafkaLinkTest(): Unit = {
    val sparkConf: SparkConf = new SparkConf().setMaster("local[2]").setAppName("JavaDirectKafkaWordCount")
    val ssc: StreamingContext = new StreamingContext(sparkConf, Durations.seconds(2))
    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> "192.168.90.130:9092",
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> "spark-streaming-group",
      "auto.offset.reset" -> "latest",
      "enable.auto.commit" -> (false: java.lang.Boolean)
    )
    val topics = Array("spark-streaming")
    val stream = KafkaUtils.createDirectStream[String, String](
      ssc,
      PreferConsistent,
      Subscribe[String, String](topics, kafkaParams)
    )

    stream.map(record => (record.key, record.value))

    stream.print()

    // 启动流计算环境StreamingContext并等待它"完成"
    ssc.start()
    // 等待作业完成
    ssc.awaitTermination()
  }
}
