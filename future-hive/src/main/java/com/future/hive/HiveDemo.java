package com.future.hive;

import com.alibaba.druid.pool.DruidDataSource;
import org.junit.Test;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

/**
 * @author tangqiang 1071180223@qq.com
 * @date 2018/07/02
 */
public class HiveDemo {
    
    private static String driverName = "org.apache.hive.jdbc.HiveDriver";
    //    private static String url = "jdbc:hive2://192.168.61.132:10000/default";
    /**
     * 高可用地址
     */
    private static String url = "jdbc:hive2://ebmas-01:2181,ebmas-02:2181,ebmas-03:2181/tang;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2";
    //    private static String url = "jdbc:hive2://hdp-2.hadoop:2181,hdp-1.hadoop:2181/default;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2";
    private static String user = "hive";
    private static String password = "hive";
    
    public static Connection conn = null;
    public static Statement stat = null;
    
    @Test
    public void test1() {
        DataSourceBuilder factory = DataSourceBuilder.create()
                .driverClassName(driverName)
                .url(url)
                .username(user)
                .password(password)
                .type(DruidDataSource.class);
        DataSource dataSource = factory.build();
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        List<Map<String, Object>> mapList = jdbcTemplate.queryForList("SHOW PARTITIONS test_table");
        System.out.println();
    }
    
    @Test
    public void test2() throws Exception {
        Class.forName(driverName);
        conn = DriverManager.getConnection(url, user, password);
        stat = conn.createStatement();
        ResultSet resultSet = stat.executeQuery("SHOW PARTITIONS test_table");
        while (resultSet.next()) {
            String partation = resultSet.getString(1);
            System.out.println(partation);
        }
        stat.close();
        conn.close();
    }
}
