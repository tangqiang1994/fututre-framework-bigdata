package com.future.kafka.test;

import com.future.kafka.KafkaApplication;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author tangqiang
 * @date 2018/08/28
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = KafkaApplication.class)
public class KafkaTest {
    
    @Autowired
    private KafkaTemplate<String, String> template;
    
    @Before
    public void before() throws Exception {
    }
    
    @After
    public void after() throws Exception {
    }
    
    @Test
    public void sendMsg() throws Exception {
        template.send("spark-streaming", null, "hello world");
    }
}
