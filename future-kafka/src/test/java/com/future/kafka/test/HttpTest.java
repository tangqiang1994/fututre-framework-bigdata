package com.future.kafka.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author tangqiang
 * @date 2019-03-11
 */
public class HttpTest {

    private static final Logger logger = LoggerFactory.getLogger(HttpTest.class);

    private static final String url = "http://127.0.0.1:8080/send";

    @Test
    public void test1() throws IOException {
        Map<String, String> content = new HashMap<>();
        content.put("topic", "spark-streaming");
        content.put("key", "test");
        content.put("data", "lishi");
        ObjectMapper objectMapper = new ObjectMapper();
        String value = objectMapper.writeValueAsString(content);

        //1.创建OkHttpClient实例对象
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS).build();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), value);
        //2.创建一个请求
        Request request = new Request.Builder()
                .post(requestBody)
                .url(url)
                .build();
        //3.发起请求
//        okHttpClient.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                logger.info("失败", e);
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                logger.info("成功");
//            }
//        });

        Response response = okHttpClient.newCall(request).execute();
        System.out.println(response.body().string());
    }
}
