package com.future.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;

/**
 * @author tangqiang 1071180223@qq.com
 * @date 2018/06/21
 */
@EnableKafka
@Configuration
public class KafkaConfig {

    public static final Logger logger = LoggerFactory.getLogger(KafkaConfig.class);

//    @KafkaListener(id = "hello", topics = "hello")
//    public void listenT1(ConsumerRecord<?, ?> cr) {
//        logger.info("{} - {} : {}", cr.topic(), cr.key(), cr.value());
//    }
}
