package com.future.kafka.bean;

/**
 * @author tangqiang
 * @date 2019-03-11
 */
public class KafkaParam {
    private String topic;

    private String key;

    private String data;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
