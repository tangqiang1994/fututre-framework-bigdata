package com.future.kafka;

import com.future.kafka.bean.KafkaParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author tangqiang 1071180223@qq.com
 * @date 2018/06/21
 */
@Controller
public class SampleController {

    @Autowired
    private KafkaTemplate<String, String> template;

    @RequestMapping("/send")
    @ResponseBody
    String send(@RequestBody KafkaParam param) {
        //key可以为空(template.send(topic, data);)
        template.send(param.getTopic(), param.getKey(), param.getData());
        return "success";
    }
}
