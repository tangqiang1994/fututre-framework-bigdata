package com.future.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author tangqiang 1071180223@qq.com
 * @date 2018/06/21
 */
@SpringBootApplication
public class KafkaApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(KafkaApplication.class, args);
    }
}
