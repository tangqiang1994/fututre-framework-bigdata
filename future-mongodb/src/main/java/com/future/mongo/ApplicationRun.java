package com.future.mongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author tangqiang
 * @date 2018/08/29
 */
@SpringBootApplication
public class ApplicationRun {
    
    public static void main(String[] args) {
        SpringApplication.run(ApplicationRun.class, args);
    }
}
