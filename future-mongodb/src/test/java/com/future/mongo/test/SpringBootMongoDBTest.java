package com.future.mongo.test;

import com.future.mongo.ApplicationRun;
import com.future.mongo.bean.User;
import com.future.mongo.dao.UserRepositoryDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * @author tangqiang
 * @date 2018/08/29
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ApplicationRun.class)
public class SpringBootMongoDBTest {
    
    @Autowired
    UserRepositoryDao userRepositoryDao;
    
    
    @Test
    public void testInsert() {
        User user = new User();
        user.setId(66666);
        user.setAge(24);
        user.setName("tq");
        user.setSex("man");
        User insert = userRepositoryDao.insert(user);
        System.out.println(insert);
    }
    
    
    @Test
    public void testFind() {
        List<User> userList = userRepositoryDao.findByName("tq");
        System.out.println(userList);
    }
    
    @Test
    public void testDelete() {
        userRepositoryDao.deleteAll();
    }
}
