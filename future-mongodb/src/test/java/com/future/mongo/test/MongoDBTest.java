package com.future.mongo.test;

import com.future.mongo.bean.User;
import com.mongodb.MongoClient;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

/**
 * @author tangqiang
 * @date 2018/08/29
 */
public class MongoDBTest {
    
    public static void main(String[] args) {
        //可以不指定默认就是这个
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        MongoOperations mongoOps = new MongoTemplate(mongoClient, "future");
    
        User user = new User();
        user.setId(66666);
        user.setAge(24);
        user.setName("tq");
        user.setSex("man");
        mongoOps.insert(user);
        
        Criteria criteria = Criteria.where("name").is("tq");
        System.out.println(mongoOps.findOne(new Query(criteria), User.class).toString());
        
        mongoOps.dropCollection("user");
    }
}
