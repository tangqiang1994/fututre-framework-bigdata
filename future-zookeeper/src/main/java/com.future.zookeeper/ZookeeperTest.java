package com.future.zookeeper;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tangqiang 1071180223@qq.com
 * @date 2018/07/11
 */
public class ZookeeperTest {
    
    private static ZooKeeper zk = null;
    
    public static void main(String[] args) throws Exception {
        zk = new ZooKeeper("hdp-2:2181", 2000, null);
//        create("/test", "hello".getBytes());
        if (zk != null) {
            List<String> zooChildren = zk.getChildren("/", false);
            System.out.println("Znodes of '/': ");
            for (String child : zooChildren) {
                System.out.println(child);
            }
        }
//        byte[] data = zk.getData("/test", null, null);
//        String value = String.valueOf(data);
//        System.out.println(value);
    }
    
    public static void create(String path, byte[] data) throws KeeperException, InterruptedException {
        zk.create(path, data, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
    }
}
